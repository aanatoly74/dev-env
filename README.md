# Ansible Stuff to Install Development Environment #

## Roles ##
All roles intended to run on localhost.

### upd-ansible ###
Updates default xenial ansible 2.0 to vendor's latest 2.4. It needed for other roles.

Intended for all machines

### liveu-sdk ###
Installs LiveU SDK - development environment - sufficient to build every LiveU product.

Intended for developer's workstations, gitlab runners and mr.burns hounds.

### liveu-env ###
Configures a comp for LiveU environment - apt proxy, maybe dns, ntp, cron, sw updates.

Intended for developer's workstations, gitlab runners and mr.burns hounds.

### extra-artwork ###
Installs extra art work - backgrounds, icons, GUI themes, cursors etc

Intended for developer's workstations running Cinnamon


## Requirements ##
Password-less sudo for running user. `root` has it by default

To enable password-less sudo for user `me`
```bash
sudo bash  # become root
echo "me ALL=(ALL) NOPASSWD: ALL" >  /etc/sudoers.d/liveu
chmod 0440 /etc/sudoers.d/liveu
```

## Usage ##
### Install ansible 2.4 ###
If you installed from PXE you already have ansible 2.4 and can skip this step.
```bash
sudo apt-get update
sudo apt-get install ansible
ansible-pull -U http://gitlab/infra/dev-env -i hosts upd-ansible.yml
```
### Run any role ###
To install developer's workstation run these commands
```bash
ansible-pull -U http://gitlab/infra/dev-env -i hosts liveu-env.yml
ansible-pull -U http://gitlab/infra/dev-env -i hosts liveu-sdk.yml
ansible-pull -U http://gitlab/infra/dev-env -i hosts extra-artwork.yml
```
